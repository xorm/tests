module gitea.com/xorm/tests

go 1.13

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190707035753-2be1aa521ff4
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.0+incompatible
	github.com/lib/pq v1.0.0
	github.com/mattn/go-oci8 v0.0.0-20191108001511-cbd8d5bc1da0
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/shopspring/decimal v0.0.0-20191009025716-f1972eb1d1f5 // indirect
	github.com/ziutek/mymysql v1.5.4
	xorm.io/xorm v1.0.1
)
